# 机械组培训第二讲——SolidWorks的基本操作

## 一. SolidWorks的安装

机械组使用的版本是SolidWorks2022
[安装地址与教程](https://note.youdao.com/s/379s3TI6)
（从淘宝买的，失效了自己买一个就行）

## 二. SolidWorks的使用

[B站视频教程](【SOLIDWORKS 教学 精品教程 | 2024年新修 | B站 点赞 播放 收藏 NO.1】 https://www.bilibili.com/video/BV1iw411Z7HZ/?share_source=copy_web&vd_source=089a0f744bb902d2383a37c7f6338a7d)
非常好使，基本一看就会

## 三. 学习思路与技巧指点

### 3.1 草图

<img src="./SolidWorks的基本操作.assets/caotu.PNG" alt="caotu" style="zoom:50%;" />

草图是零件绘制的基础，先从平面上画出草图，才能通过“拉伸”（即转换实体引用）获得零件

草图绘制过程中，最重要的部分是标注，标注要求限定住整个零件的形状和不同区域间的距离，不能有未定义的部分
标注的手法也是最值得练习的，比如怎么选中能标注边，怎么选中两条边能标注夹角，看视频可能不明确，需要自己多加练习。

### 3.2 零件

<img src="./SolidWorks的基本操作.assets/lingjian.png" alt="lingjian" style="zoom:50%;" />

由草图拉伸，切除，旋转形成，是基本的部件

学习时，可以注意能提高效率的操作，如阵列，复制

### 3.3 装配体

<img src="./SolidWorks的基本操作.assets/zhuangpeiti.png" alt="zhuangpeiti" style="zoom:50%;" />

由零件拼接而成的设备

绘制时注意零件之间的对应关系和物理连接

### 3.4 工程图

<img src="./SolidWorks的基本操作.assets/gongchengtu.PNG" alt="gongchengtu" style="zoom:50%;" />

将三维转二维工程图，用于指导生产加工。工程图包含了投影视图、剖视图、标注、公差等功能。



